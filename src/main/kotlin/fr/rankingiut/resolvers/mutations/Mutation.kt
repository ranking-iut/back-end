package fr.rankingiut.resolvers.mutations

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.mindrot.jbcrypt.BCrypt
import org.springframework.stereotype.Component

@Component
class Mutation : GraphQLMutationResolver {
    fun hashPassword(input: String) = BCrypt.hashpw(input, BCrypt.gensalt())
    fun checkPassword(input: String) = BCrypt.checkpw("Hello there", input).toString()
}