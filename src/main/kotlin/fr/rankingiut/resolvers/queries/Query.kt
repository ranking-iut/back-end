package fr.rankingiut.resolvers.queries

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.mindrot.jbcrypt.BCrypt
import org.springframework.stereotype.Component

@Component
class Query : GraphQLQueryResolver {
    fun version() = "1.0.0"
}